package com.ioasys.empresaskotlin.app

import android.app.Application
import com.ioasys.empresaskotlin.di.applicationModule
import com.ioasys.empresaskotlin.di.dataModule
import com.ioasys.empresaskotlin.di.domainModule
import com.ioasys.empresaskotlin.di.presentationModule
import org.koin.android.ext.android.startKoin

class MyApplication : Application(){
    override fun onCreate() {
        super.onCreate()
        // Start Koin
        startKoin(this, listOf(applicationModule, dataModule, domainModule, presentationModule))
    }
}