package com.ioasys.empresaskotlin.data

import com.ioasys.empresaskotlin.data.model.login.HeaderApi

interface SharedPreferencies {
    fun saveHeaders(headers: HeaderApi)

    fun loadHeaders(): HeaderApi
}