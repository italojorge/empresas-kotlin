package com.ioasys.empresaskotlin.data

import android.content.Context
import com.ioasys.empresaskotlin.data.model.login.HeaderApi

class SharedPreferenciesImpl(val context: Context) : SharedPreferencies {

    private val ARQUIVO_HEADERS = "arquivo.headers"
    private val preferencies = context.getSharedPreferences(ARQUIVO_HEADERS, 0)
    private val editor = preferencies.edit()

    override fun saveHeaders(headers: HeaderApi) {
        editor.putString("access-token",headers.access_token)
        editor.putString("uid",headers.uid)
        editor.putString("client",headers.client)
        editor.apply()
    }

    override fun loadHeaders(): HeaderApi {
        return HeaderApi(
            preferencies.getString("access-token", "") ?: "",
            preferencies.getString("uid", "") ?: "",
            preferencies.getString("client", "") ?: ""
        )
    }

}