package com.ioasys.empresaskotlin.data.model.login

data class AuthRequest(val success: Boolean)