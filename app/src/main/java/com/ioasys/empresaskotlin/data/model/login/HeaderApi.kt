package com.ioasys.empresaskotlin.data.model.login

data class HeaderApi(
    var access_token: String,
    var uid: String,
    var client: String
)