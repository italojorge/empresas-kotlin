package com.ioasys.empresaskotlin.data.model.login

data class User(
    var email: String = "",
    var password: String = ""
)