package com.ioasys.empresaskotlin.data.model.search

data class Enterprise(
    var enterprise_name: String? = "",
    var photo: String? = "",
    var description: String? = "",
    var country: String? = "",
    var enterprise_type: EnterpriseType = EnterpriseType()
)