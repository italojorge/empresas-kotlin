package com.ioasys.empresaskotlin.data.model.search

data class EnterprisesList(var enterprises: List<Enterprise>)
