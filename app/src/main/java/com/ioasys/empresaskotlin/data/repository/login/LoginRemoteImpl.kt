package com.ioasys.empresaskotlin.data.repository.login

import com.ioasys.empresaskotlin.data.SharedPreferencies
import com.ioasys.empresaskotlin.data.model.login.AuthRequest
import com.ioasys.empresaskotlin.data.model.login.HeaderApi
import com.ioasys.empresaskotlin.data.model.login.User
import com.ioasys.empresaskotlin.data.service.LoginWebService
import com.ioasys.empresaskotlin.domain.repository.login.LoginRemoteRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import retrofit2.Response

class LoginRemoteImpl( private val webService: LoginWebService,
                       private val sharedPreferencies: SharedPreferencies) : LoginRemoteRepository {

    override fun login(email: String,
                       password: String,
                       isSuccess: ()-> Unit,
                       isFailure: (it: String)-> Unit)
            : DisposableObserver<Response<AuthRequest>> {
        return webService.login(
            User(
                email = email,
                password = password
            )
        ).toObservable()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(object : DisposableObserver<Response<AuthRequest>>() {
                override fun onComplete() {}

                override fun onNext(response: Response<AuthRequest>) {
                    if (response.isSuccessful) {
                        val headers = response.headers()
                        sharedPreferencies.saveHeaders(
                            HeaderApi(
                                headers["access-token"] ?: "",
                                headers["uid"] ?: "",
                                headers["client"] ?: ""
                            )
                        )
                        isSuccess()
                    } else {
                        isFailure("Usuário e/ou senha inválidos")
                    }
                }

                override fun onError(e: Throwable) {
                    isFailure("Falha na rede")
                }

            })
    }
}