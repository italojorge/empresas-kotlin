package com.ioasys.empresaskotlin.data.service

import com.ioasys.empresaskotlin.data.model.login.AuthRequest
import com.ioasys.empresaskotlin.data.model.login.User
import com.ioasys.empresaskotlin.data.model.search.Enterprise
import io.reactivex.Flowable
import retrofit2.Response
import retrofit2.http.*

interface LoginWebService {
    @POST(PATH_SIGN_IN)
    fun login(@Body user: User): Flowable<Response<AuthRequest>>

    companion object {
        const val BASE_URL_PHOTO = "http://empresas.ioasys.com.br"
        const val BASE_URL = "http://empresas.ioasys.com.br/api/v1/"

        const val PATH_SIGN_IN = "users/auth/sign_in"
        const val PATH_ENTERPRISES = "enterprises"

        const val FIELD_NAME = "name"

    }
}