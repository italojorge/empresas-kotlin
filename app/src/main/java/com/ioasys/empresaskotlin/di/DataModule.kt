package com.ioasys.empresaskotlin.di

import com.ioasys.empresaskotlin.data.SharedPreferencies
import com.ioasys.empresaskotlin.data.SharedPreferenciesImpl
import com.ioasys.empresaskotlin.data.createWebService
import com.ioasys.empresaskotlin.data.repository.login.LoginRemoteImpl
import com.ioasys.empresaskotlin.data.service.LoginWebService
import com.ioasys.empresaskotlin.domain.repository.login.LoginRemoteRepository
import org.koin.dsl.module.module

val dataModule = module {
    //web service
    single { createWebService<LoginWebService>() }

    single { SharedPreferenciesImpl(get()) as SharedPreferencies }

    single { LoginRemoteImpl(get(),get()) as LoginRemoteRepository }

}
