package com.ioasys.empresaskotlin.di

import com.ioasys.empresaskotlin.data.repository.login.LoginRemoteImpl
import com.ioasys.empresaskotlin.domain.repository.login.LoginRemoteRepository
import com.ioasys.empresaskotlin.domain.usecase.login.LoginUseCase
import com.ioasys.empresaskotlin.domain.usecase.login.LoginUseCaseImpl
import org.koin.dsl.module.module

val domainModule = module {

    single { LoginUseCaseImpl(get()) as LoginUseCase}

}