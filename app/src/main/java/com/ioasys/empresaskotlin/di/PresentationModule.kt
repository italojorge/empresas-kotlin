package com.ioasys.empresaskotlin.di

import com.ioasys.empresaskotlin.presentation.login.LoginViewModel
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val presentationModule = module {
    /* LOGIN */
    viewModel { LoginViewModel(get()) }
}