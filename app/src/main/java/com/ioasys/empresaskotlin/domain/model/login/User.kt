package com.ioasys.empresaskotlin.domain.model.login

data class User(
    var email: String = "",
    var password: String = ""
)