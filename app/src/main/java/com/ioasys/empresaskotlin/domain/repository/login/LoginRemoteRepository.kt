package com.ioasys.empresaskotlin.domain.repository.login

import com.ioasys.empresaskotlin.data.model.login.AuthRequest
import io.reactivex.observers.DisposableObserver
import retrofit2.Response

interface LoginRemoteRepository {
    fun login(
        email: String,
        password: String,
        isSuccess: () -> Unit,
        isFailure: (it: String) -> Unit

    ): DisposableObserver<Response<AuthRequest>>
}