package com.ioasys.empresaskotlin.domain.repository.search

import com.ioasys.empresaskotlin.data.model.login.HeaderApi
import com.ioasys.empresaskotlin.data.model.search.Enterprise
import io.reactivex.Observable

interface SearchRemoteRepository {
    fun searchEnterprises(query: String, headerApi: HeaderApi): Observable<List<Enterprise>>
}