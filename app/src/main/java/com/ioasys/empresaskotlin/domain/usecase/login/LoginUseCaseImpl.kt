package com.ioasys.empresaskotlin.domain.usecase.login

import com.ioasys.empresaskotlin.data.SharedPreferencies
import com.ioasys.empresaskotlin.data.model.login.AuthRequest
import com.ioasys.empresaskotlin.data.model.login.HeaderApi
import com.ioasys.empresaskotlin.domain.repository.login.LoginRemoteRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import retrofit2.Response

class LoginUseCaseImpl(private val loginRemoteRepository: LoginRemoteRepository) : LoginUseCase{

    override fun login(
        email: String,
        password: String,
        isSuccess: () -> Unit,
        isFailure: (it: String) -> Unit
    ): DisposableObserver<Response<AuthRequest>> {
        return loginRemoteRepository.login(email, password, { isSuccess() }, {isFailure(it)})

    }
}