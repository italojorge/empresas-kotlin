package com.ioasys.empresaskotlin.presentation.login

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.databinding.BindingAdapter
import android.support.design.widget.TextInputLayout
import com.ioasys.empresaskotlin.data.model.login.AuthRequest
import com.ioasys.empresaskotlin.domain.usecase.login.LoginUseCase
import com.ioasys.empresaskotlin.presentation.ViewState
import com.ioasys.empresaskotlin.ui.isEmail
import com.ioasys.empresaskotlin.ui.isPassword
import io.reactivex.observers.DisposableObserver
import retrofit2.Response

class LoginViewModel(private val loginUseCase: LoginUseCase) : ViewModel() {

    private lateinit var loginSubscriber: DisposableObserver<Response<AuthRequest>>

    private val viewState = MutableLiveData<ViewState<String>>()
    val passwordError = MutableLiveData<String?>()
    val emailError = MutableLiveData<String?>()

    val inputEmail = MutableLiveData<String?>()
    val inputPassword = MutableLiveData<String?>()

    fun getState(): LiveData<ViewState<String>> = viewState

    fun onLoginClicked() {

        val email: String = inputEmail.value ?: ""
        val password: String = inputPassword.value ?: ""

        verificaEmaileSenha(email, password)

        if (email.isEmail() && password.isPassword()) {

            viewState.postValue(ViewState.loading())

            loginSubscriber = loginUseCase.login(
                email,
                password,
                { viewState.postValue(ViewState.success()) },
                { viewState.postValue(ViewState.error(Exception(it))) }
            )
        }
    }

    private fun verificaEmaileSenha(email: String, password: String) {
        if (email.isEmail()
        ) {
            emailError.value = null
        } else {
            emailError.value = "Digite um endereço de email válido"
        }

        if (password.isPassword()) {
            passwordError.value = null

        } else {
            passwordError.value = "A senha deve conter no mínimo 5 dígitos"
        }
    }

    companion object {
        @BindingAdapter("app:errorText")
        @JvmStatic
        fun setErrorMessage(view: TextInputLayout, errorMessage: String?) {
            view.error = errorMessage
        }
    }

    override fun onCleared() {
        loginSubscriber.dispose()
        super.onCleared()
    }
}
