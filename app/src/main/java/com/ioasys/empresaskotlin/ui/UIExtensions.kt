package com.ioasys.empresaskotlin.ui

import android.util.Patterns

fun String.isEmail() = Patterns.EMAIL_ADDRESS.matcher(this).matches()

fun String.isPassword() = this.length > 4
