package com.ioasys.empresaskotlin.ui.login

import android.arch.lifecycle.Observer
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.ioasys.empresaskotlin.R
import com.ioasys.empresaskotlin.databinding.ActivityLoginBinding
import com.ioasys.empresaskotlin.presentation.login.LoginViewModel
import com.ioasys.empresaskotlin.presentation.Status
import kotlinx.android.synthetic.main.activity_login.*
import org.koin.android.viewmodel.ext.android.viewModel

class LoginActivity : AppCompatActivity() {

    private val viewModel: LoginViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityLoginBinding = DataBindingUtil
            .setContentView(this, R.layout.activity_login)
        binding.vm = viewModel
        binding.setLifecycleOwner(this)

        viewModel.getState().observe(this, Observer { viewState ->
            progressBar.visibility = View.GONE
            login_emailSenhaInvalidos_textView.text = ""

            when (viewState?.status) {
                Status.SUCCESS -> {
                    login_emailSenhaInvalidos_textView.text = "Logado"
                }

                Status.FAILURE -> {
                    viewState.throwable?.let {
                        login_emailSenhaInvalidos_textView.text = it.message
                    }
                }

                Status.LOADING -> {
                    progressBar.visibility = View.VISIBLE
                }

                else -> {
                }
            }
        })
    }
}
